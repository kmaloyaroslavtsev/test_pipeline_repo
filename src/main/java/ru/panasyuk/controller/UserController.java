package ru.panasyuk.controller;

/**
 * Created by Danila on 02.01.2018.
 */
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.entity.User;
import ru.panasyuk.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity getUsers() {
        logger.debug("REST request to get users");
        return new ResponseEntity(userService.findAll(), HttpStatus.OK);
    }

    @PostMapping("/user")
    public ResponseEntity createUser(@RequestBody User user) {
        logger.debug("REST request to create user {}", user);
        user = userService.saveUser(user);
        return new ResponseEntity(user, HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
        logger.debug("REST request to delete user id {} ", id);
        userService.deleteUser(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}