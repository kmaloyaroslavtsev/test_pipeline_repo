package ru.panasyuk.entity;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by Danila on 27.03.2018.
 */
@Entity
@Table(name = "transactions")
public class Transaction {
    @Id
    @SequenceGenerator(
            name = "transactions_sequence",
            sequenceName = "transactions_sequence"
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "transactions_sequence")
    private Long id;

    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @NotNull
    @Column(name = "creation_date", nullable = false)
    private LocalDateTime creationDate = LocalDateTime.now();

    @Column
    private Boolean approved;

    @OneToOne
    @JoinColumn(name = "from_id")
    private Account accountFrom;
    @OneToOne

    @JoinColumn(name = "to_id")
    private Account accountTo;

    @Column
    private BigDecimal amount;

    @Type(type = "org.hibernate.type.LocalDateTimeType")
    @Column(name = "approval_date", nullable = false)
    private LocalDateTime approvalDate = LocalDateTime.now();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public Account getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(Account accountFrom) {
        this.accountFrom = accountFrom;
    }

    public Account getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(Account accountTo) {
        this.accountTo = accountTo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public LocalDateTime getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(LocalDateTime approvalDate) {
        this.approvalDate = approvalDate;
    }
}
