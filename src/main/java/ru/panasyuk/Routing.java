package ru.panasyuk;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Danila on 09.03.2017.
 */
@Controller
public class Routing {
    @RequestMapping({
            "/list",
            "/list/**",
            "/users",
            "/transactions",
            "/h2-console",
            "/h2-console/**",
    })
    public String index(HttpServletRequest request) {
        return "forward:/";
    }
}
