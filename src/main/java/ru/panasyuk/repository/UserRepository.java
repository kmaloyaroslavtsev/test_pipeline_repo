package ru.panasyuk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.panasyuk.entity.User;

/**
 * Created by Danila on 27.03.2018.
 */
public interface UserRepository  extends JpaRepository<User, Long> {

}
