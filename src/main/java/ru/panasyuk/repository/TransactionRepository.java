package ru.panasyuk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.panasyuk.entity.Account;
import ru.panasyuk.entity.Transaction;

/**
 * Created by Danila on 27.03.2018.
 */
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
