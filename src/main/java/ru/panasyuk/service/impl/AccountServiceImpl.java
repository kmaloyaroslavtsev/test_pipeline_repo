package ru.panasyuk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.panasyuk.entity.Account;
import ru.panasyuk.repository.AccountRepository;
import ru.panasyuk.service.AccountService;

import java.util.List;

/**
 * Created by Danila on 02.01.2018.
 */
@Component
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;
    
    @Override
    public List<Account> findAll(){
        return accountRepository.findAll();
    }

    @Override
    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public void deleteAccount(Long id) {
        accountRepository.delete(id);
    }

    
}
