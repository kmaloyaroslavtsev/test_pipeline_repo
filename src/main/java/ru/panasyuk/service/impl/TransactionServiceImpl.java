package ru.panasyuk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.panasyuk.entity.Transaction;
import ru.panasyuk.repository.TransactionRepository;
import ru.panasyuk.service.TransactionService;

import java.util.List;

/**
 * Created by Danila on 02.01.2018.
 */
@Component
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;
    
    @Override
    public List<Transaction> findAll(){
        return transactionRepository.findAll();
    }

    @Override
    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public void deleteTransaction(Long id) {
        transactionRepository.delete(id);
    }

    
}
