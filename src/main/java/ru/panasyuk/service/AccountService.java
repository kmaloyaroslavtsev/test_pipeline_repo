package ru.panasyuk.service;

import ru.panasyuk.entity.Account;
import ru.panasyuk.entity.User;

import java.util.List;

/**
 * Created by Danila on 02.01.2018.
 */
public interface AccountService {

    List<Account> findAll();

    Account saveAccount(Account account);

    void deleteAccount(Long id);
}
