package ru.panasyuk.service;

import ru.panasyuk.entity.User;
import ru.panasyuk.exception.WrongArgumentException;

import java.util.List;
import java.util.Set;

/**
 * Created by Danila on 02.01.2018.
 */
public interface UserService {

    List<User> findAll();

    User saveUser(User user);

    void deleteUser(Long id);
}
