package ru.panasyuk.service;

import ru.panasyuk.entity.Transaction;

import java.util.List;

/**
 * Created by Danila on 02.01.2018.
 */
public interface TransactionService {
    
    List<Transaction> findAll();

    Transaction saveTransaction(Transaction transaction);

    void deleteTransaction(Long id);
}
