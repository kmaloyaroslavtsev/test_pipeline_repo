insert into users values (1, 'Helen','Jones');
insert into account values (1,'Payment');
insert into user_account values (1,1);

insert into users values (2, 'Bob','Saget');
insert into account values (2,'Savings');
insert into user_account values (2,2);
insert into account values (3,'Investment');
insert into user_account values (2,3);
insert into account values (4,'Payment');
insert into user_account values (2,4);

insert into transactions values (1, now(), true,  1, 2, 9.50 , now());
insert into transactions values (2, now(), true,  2, 1, 10.00, now());
insert into transactions values (3, now(), true,  2, 3, 4.00 , now());
insert into transactions values (4, now(), true,  3, 1, 6.66 , now());
insert into transactions values (5, now(), true,  4, 2, 12.12, now());
insert into transactions values (6, now(), false, 4, 3, 4.23,  now());

select setval('users_sequence', (select max(id)+1 from users), false);
select setval('account_sequence', (select max(id)+1 from account), false);
select setval('transactions_sequence', (select max(id)+1 from transactions), false);