CREATE ALIAS setval FOR "org.h2.dynamic.Function.setVal";
CREATE TABLE account
(
    id bigint NOT NULL,
    name character varying(64),
    CONSTRAINT account_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE account_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE transactions
(
    id bigint NOT NULL,
    creation_date timestamp NOT NULL,
    approved boolean NOT NULL,
    from_id bigint NOT NULL,
    to_id bigint NOT NULL,
    amount numeric(19, 2) NOT NULL,
    approval_date timestamp,
    CONSTRAINT transactions_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE transactions_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE users
(
    id bigint NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE users_sequence
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE user_account
(
    user_id bigint NOT NULL,
    account_id bigint NOT NULL,
    CONSTRAINT user_account_pkey PRIMARY KEY (user_id, account_id)
);