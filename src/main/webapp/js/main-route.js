app.config(function ($httpProvider, $stateProvider, $urlRouterProvider,$locationProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'js/home/home.html',
                controller: 'homeController'
            })
            .state('users', {
                url: '/users',
                templateUrl: 'js/user/users.html',
                controller: 'usersController'
            })
            .state('transactions', {
                url: '/transactions',
                templateUrl: 'js/transaction/transactions.html',
                controller: 'transactionsController'
            })
            .state('account', {
                url: '/account/:id',
                templateUrl: 'js/transaction/transactions.html',
                controller: 'transactionsController'
            });
    }
);
