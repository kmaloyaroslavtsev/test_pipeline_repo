angular.module('app').factory('User', function($resource) {
  return $resource('/api/users/:id');
});

angular.module('app').service('UserService', function f(User) {

    return {
            getUsers: function (callback) {
                var cb = callback || angular.noop;

                return User.query(
                    function () {
                        return cb();
                    },
                    function (err) {
                        return cb(err);
                    }.bind(this)).$promise;
            },
            getUser: function (id, callback) {
                var cb = callback || angular.noop;

                return User.get({id: id},
                    function (response) {
                        return cb(response);
                    },
                    function (err) {
                        return cb(err);
                    }.bind(this)).$promise;
            },
            saveUser: function (user, callback) {
                var cb = callback || angular.noop;

                return User.save(user,
                    function () {
                        return cb(user);
                    },
                    function (err) {
                        return cb(err);
                    }.bind(this)).$promise;
            }
}
});

