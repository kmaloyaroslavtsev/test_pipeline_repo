angular.module('app').controller('usersController',  function ($scope, UserService) {
    UserService.getUsers().then(function (data) {
        $scope.users = data;
    });
});